﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockBullet : MonoBehaviour
{
    Vector3 startPosition;

    public Vector2 timeToHit;

    float m_randomTime;
    float m_lifeTime;
    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.localPosition;

        m_randomTime = Random.Range(timeToHit.x, timeToHit.y);
        m_lifeTime = 0;
    }

    private void OnDisable() 
    {
        Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        m_lifeTime += Time.deltaTime;
        
        float tempValue;
        if(m_lifeTime > m_randomTime)
        {
            tempValue = 0;
        }
        else
        {
            tempValue = (m_randomTime - m_lifeTime) / m_randomTime;
        }

        transform.localPosition = startPosition * tempValue;
    }
}
