﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockBulletParticles : MonoBehaviour
{
    float m_lifeTime;
    // Start is called before the first frame update
    void Start()
    {
        m_lifeTime = 0;
    }

    private void OnDisable() 
    {
        Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        m_lifeTime += Time.deltaTime;

        if(m_lifeTime > 1)
            Destroy(this.gameObject);
    }
}
