﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniGameClock : MonoBehaviour
{  
     public MiniGameManager gameManager;
    public float startValue;
    public float startAngle;

    public Vector2  bulletSpawnTime;
    public Vector2 bulletSpwanPos;
    public List<GameObject> spwanObjectList;
    public float gameTime;

    public float rotateSpeed;
    public Image progressImage;
    public GameObject redPointer;
    public GameObject blackPointer;

    public GameObject bulletParticles;

    float m_spawnTime;
    float m_gameProgress;

    bool m_gameClear;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        m_gameProgress = startValue;
        progressImage.fillAmount = m_gameProgress;
        redPointer.transform.localEulerAngles = new Vector3(180, 0, startAngle);
        blackPointer.transform.localEulerAngles = Vector3.zero;
        m_gameClear = false;

        m_spawnTime = Random.Range(bulletSpawnTime.x, bulletSpawnTime.y);
    }

    // Update is called once per frame
    void Update()
    {
        if(m_gameClear)
            return;

        m_gameProgress += Time.deltaTime / gameTime;

        if(m_gameProgress >= 1)
        {
            m_gameProgress = 1;
            m_gameClear = true;
        }
        float pointerAngle = m_gameProgress * 360 ;
        redPointer.transform.localEulerAngles = new Vector3(180, 0, pointerAngle);

        progressImage.fillAmount = m_gameProgress;

        if(m_gameClear)
        {
            StartCoroutine(GameClearEffect());
            return;
        }
            
        if (Input.GetKey (KeyCode.A))
        {
            blackPointer.transform.Rotate(0, 0, rotateSpeed * Time.deltaTime);
        }
        else if (Input.GetKey (KeyCode.D)) 
        {
            blackPointer.transform.Rotate(0, 0, rotateSpeed * -1 * Time.deltaTime);
        }
        else if (Input.GetKey (KeyCode.LeftArrow)) 
        {
            blackPointer.transform.Rotate(0, 0, rotateSpeed * Time.deltaTime);
        }
        else if (Input.GetKey (KeyCode.RightArrow))
        {
            blackPointer.transform.Rotate(0, 0, rotateSpeed * -1 * Time.deltaTime);
        }


        m_spawnTime -= Time.deltaTime;
        if(m_spawnTime > 0)
            return;

        m_spawnTime = Random.Range(bulletSpawnTime.x, bulletSpawnTime.y);

        Vector3 spawnPos = new Vector3(Random.Range(bulletSpwanPos.x * -1, bulletSpwanPos.x), Random.Range(bulletSpwanPos.y * -1, bulletSpwanPos.y), 0);

        if(Random.Range(0, 10) < 5)
        {
            if(spawnPos.x > 0)
            {
                spawnPos.x = bulletSpwanPos.x;
            }
            else
            {
                spawnPos.x = bulletSpwanPos.x * -1;
            }
        }
        else
        {
            if(spawnPos.y > 0)
            {
                spawnPos.y = bulletSpwanPos.y;
            }
            else
            {
                spawnPos.y = bulletSpwanPos.y * -1;
            }
        }

        int bulletID = Random.Range(0, spwanObjectList.Count);
        GameObject bullet = Instantiate(spwanObjectList[bulletID], spawnPos, Quaternion.identity, this.transform);
    }

    public void RemoveBullet(GameObject bullet)
    {
        if(m_gameClear)
            return;

        Instantiate(bulletParticles, bullet.transform.position, Quaternion.identity, this.transform);

        Destroy(bullet);
    }
    
    public void takeDamage(float damage)
    {
        if(m_gameClear)
            return;
            
        m_gameProgress -= damage;
        if(m_gameProgress < startValue)
            m_gameProgress = startValue;
    }

    IEnumerator GameClearEffect()
    {
        yield return new WaitForSeconds(1.0f);
        GameEnd();
    }

    void GameEnd()
    {
        gameManager.GameFinish(this.gameObject);
        // this.gameObject.SetActive(false);
    }
}
