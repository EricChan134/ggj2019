using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "MsgData", menuName = "ggj2019/MsgData", order = 0)]
public class MsgData : ScriptableObject {
    public List<string> questionList;
    public List<string> answersList;
    public List<int> correctAnswerIndexList;

    public List<string> chitChatList;
    public List<string> chitChatResponseList;

    public List<string> otherTextList;
    public List<string> otherSenderList;
}