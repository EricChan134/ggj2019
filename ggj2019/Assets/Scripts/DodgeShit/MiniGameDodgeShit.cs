﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MiniGameDodgeShit : MonoBehaviour
{
    public MiniGameManager gameManager;
    public Slider progressBar;
    public List<GameObject> itemList;

    public GameObject player;

    public Vector2 randomSpawnTime;
    public Vector2 itemSpownPosition;

    public float playerSpeed;

    public TextMeshProUGUI scoreText;

    int m_gameScore;
    float m_spawnTime;
    bool m_gameClear;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        m_gameScore = 0;
        progressBar.value = m_gameScore;
        m_spawnTime = Random.Range(randomSpawnTime.x, randomSpawnTime.y);
        m_gameClear = false;

        player.transform.localPosition = new Vector3(0 , -3, 0);

        scoreText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if(m_gameClear)
            return;

        if (Input.GetKey (KeyCode.A))
        {
            player.transform.localPosition += Vector3.left * playerSpeed * Time.deltaTime;
            player.transform.localEulerAngles = Vector3.zero;
        }
        else if (Input.GetKey (KeyCode.D)) 
        {
            player.transform.localPosition += Vector3.right * playerSpeed * Time.deltaTime;
            player.transform.localEulerAngles = new Vector3(0, 180 , 0);
        }
        else if (Input.GetKey (KeyCode.LeftArrow)) 
        {
            player.transform.localPosition += Vector3.left * playerSpeed * Time.deltaTime;
            player.transform.localEulerAngles = Vector3.zero;
        }
        else if (Input.GetKey (KeyCode.RightArrow))
        {
            player.transform.localPosition += Vector3.right * playerSpeed * Time.deltaTime;
            player.transform.localEulerAngles = new Vector3(0, 180 , 0);
        }

        if(player.transform.localPosition.x < -6)
        {
            player.transform.localPosition = new Vector3(-6, -3, 0);
        }
        if(player.transform.localPosition.x > 6)
        {
            player.transform.localPosition = new Vector3(6, -3,0);
        }

        m_spawnTime -= Time.deltaTime;

        if(m_spawnTime > 0)
            return;

        m_spawnTime = Random.Range(randomSpawnTime.x, randomSpawnTime.y);

        float spawnPosX = Random.Range(itemSpownPosition.x, itemSpownPosition.y);

        int spawnItemID = Random.Range(0 , itemList.Count);
        GameObject gameItem = Instantiate(itemList[spawnItemID], new Vector3(spawnPosX, 6 , 0), Quaternion.identity, this.transform);
    }
    

    public void ChangeScore(int changeValue)
    {
        if(m_gameClear)
            return;

        m_gameScore += changeValue;
        if(m_gameScore < 0)
            m_gameScore = 0;
        progressBar.value = m_gameScore;

        if(changeValue > 0)
        {
            scoreText.color = Color.black;
            scoreText.text = "+";
        }
        else
        {
            scoreText.color = Color.red;
            scoreText.text = "";
        }
        scoreText.text += changeValue.ToString();

        if(m_gameScore >= 100)
        {
            m_gameClear = true;
            StartCoroutine(GameClearEffect());
        }
    }

    IEnumerator GameClearEffect()
    {
        yield return new WaitForSeconds(1.0f);
        GameEnd();
    }

    void GameEnd()
    {
        gameManager.GameFinish(this.gameObject);
        // this.gameObject.SetActive(false);
    }
}
