﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DodgeShitPlayer : MonoBehaviour
{
    public MiniGameDodgeShit gameManager;

    public int shitDamage;
    public int itemScore;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.CompareTag("DodgeShit_Shit"))
        {
            gameManager.ChangeScore(-shitDamage);
        }
        else
        {
            gameManager.ChangeScore(itemScore);
        }

        Destroy(other.gameObject);
    }
}
