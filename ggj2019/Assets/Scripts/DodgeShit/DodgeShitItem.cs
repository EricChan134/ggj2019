﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DodgeShitItem : MonoBehaviour
{
    public float lifeTime;

    public Vector2 randomSpeed;
    public Vector2 randomRotate;

    float m_speed;
    Vector3 transferDistance;

    // Start is called before the first frame update
    void Start()
    {
        m_speed = Random.Range(randomSpeed.x, randomSpeed.y);

        transferDistance = new Vector3(0 , -m_speed, 0);

        float rotateZ = Random.Range(randomRotate.x, randomRotate.y);
        transform.Rotate(0 , 0, rotateZ);
    }

    private void OnDisable() 
    {
        Destroy(this.gameObject);
    }


    // Update is called once per frame
    void Update()
    {
        lifeTime -= Time.deltaTime;

        if (lifeTime <= 0)
        {
            Destroy(this.gameObject);
        }

        transform.position += transferDistance * Time.deltaTime;
    }
}
