﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Mail : MonoBehaviour
{
    public TextMeshPro mailAddress;
    string finalAddress;

    public string GetFinalAddress() {
        return finalAddress;
    }

    public void UpdateMailAddress(Color randColor, string colorCode, string address) {
        finalAddress = colorCode + address;
        mailAddress.text = finalAddress;
        mailAddress.color = randColor;
    }
}
