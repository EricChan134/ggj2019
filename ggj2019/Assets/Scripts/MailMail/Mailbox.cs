﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Mailbox : MonoBehaviour {
    public SpriteRenderer mailboxSprite;
    public TextMeshPro mailboxAddress;
    public GameObject correctPrefab;
    public GameObject wrongPrefab;

    Color myColor;
    string myColorCode;
    string myAddress;

    public Color GetMailboxColor() {
        return myColor;
    }

    public string GetMailboxColorCode() {
        return myColorCode;
    }

    public string GetMailboxAddress() {
        return myAddress;
    }

    public string GetFinalAddress() {
        return myColorCode+myAddress;
    }

    public void UpdateMailboxUI(Color color, string colorCode, string address) {
       myColor = color;
       myColorCode = colorCode;
       myAddress = address;
       mailboxSprite.color = color;
       mailboxAddress.text = address; 
    }

    public void CorrectClick() {
        Instantiate(correctPrefab, this.transform);
    }

    public void WrongClick() {
        Instantiate(wrongPrefab, this.transform);
    }
}
