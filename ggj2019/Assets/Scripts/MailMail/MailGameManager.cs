﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MailGameManager : MonoBehaviour {

    public MiniGameManager gameManager;

    public GameObject allMailBoxesObj;
    public Mail mail;
    public List<Color> listOfColor;
    public List<string> listOfColorCode;
    public List<string> listOfAddress;


    List<Mailbox> listOfMailbox;
    bool isInit = false;

    string currentMailFinalAddress;

    public int targetNumber;

    int m_SuccessCount;
    bool m_gameClear;

    void Start() {
        if(!isInit) {
            Init();
        }
        RandomMailboxAppearance();
        NextMail();
    }

    private void OnEnable() {
        if(!isInit) {
            Init();
        }

        m_SuccessCount = 0;
        m_gameClear = false;

        RandomMailboxAppearance();
        NextMail();
    }

    void Init() {
        Transform allMailBoxesTrasn = allMailBoxesObj.transform;
        listOfMailbox = new List<Mailbox>();
        for(int i = 0 ; i < allMailBoxesTrasn.childCount ; i++) {
            listOfMailbox.Add(allMailBoxesTrasn.GetChild(i).GetComponent<Mailbox>());
        }
    }

    public void RandomMailboxAppearance() {
        List<int> mailboxIndexPool = new List<int>();
        for(int i = 0 ; i < listOfMailbox.Count ; i ++){
            mailboxIndexPool.Add(i);
        }

        for(int i = 0 ; i < listOfColor.Count ; i++) {
            for(int j = 0 ; j < listOfAddress.Count ; j++) {
                int randomIndex = Random.Range(0, mailboxIndexPool.Count);
                int mailboxIndex = mailboxIndexPool[randomIndex];
                mailboxIndexPool.RemoveAt(randomIndex);
                listOfMailbox[mailboxIndex].UpdateMailboxUI(listOfColor[i], listOfColorCode[i], listOfAddress[j]);
            }
        }
    }

    public void NextMail() {
        Color randColor = listOfColor[Random.Range(0, listOfColor.Count)];
        string randColorCode = listOfColorCode[Random.Range(0, listOfColorCode.Count)];
        string randAddress = listOfAddress[Random.Range(0, listOfAddress.Count)];
        currentMailFinalAddress = randColorCode + randAddress;
        mail.UpdateMailAddress(randColor, randColorCode, randAddress);
    }

    void Update() {
        if(m_gameClear)
            return;

        if(Input.GetMouseButtonDown(0)) {
            Debug.Log("Click");
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit)){
                Debug.Log(hit.collider.gameObject.name);
                if(hit.collider.gameObject.tag == "Mailbox") {
                    Mailbox targetMailbox = hit.collider.gameObject.GetComponent<Mailbox>();
                    if(targetMailbox.GetFinalAddress() == currentMailFinalAddress) {
                        targetMailbox.CorrectClick();
                        m_SuccessCount++;
                        NextMail();
                    }
                    else {
                        targetMailbox.WrongClick();
                    }
                }
            }
        }   

        if(m_SuccessCount >= targetNumber) {
            m_gameClear = true;
            StartCoroutine(GameClearEffect());
        }
    }

    IEnumerator GameClearEffect()
    {
        yield return new WaitForSeconds(1.0f);
        GameEnd();
    }

    void GameEnd()
    {
        gameManager.GameFinish(this.gameObject);
        // this.gameObject.SetActive(false);
    }
}  
