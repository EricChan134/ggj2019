﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    public SpriteRenderer sprite;
    public float time;
    
    // Start is called before the first frame update
    void Start() {
        StartCoroutine(selfDestroy());
    }

    IEnumerator selfDestroy() {
        yield return new WaitForSeconds(time);
        while(sprite.color.a > 0) {
            Color color = sprite.color;
            color.a -= 0.1f;
            sprite.color = color;
            yield return new WaitForEndOfFrame();
        }
        Destroy(this.gameObject);
    }
}