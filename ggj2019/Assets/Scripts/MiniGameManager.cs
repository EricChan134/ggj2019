﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameManager : MonoBehaviour
{
    public GameObject[] miniGameList = new GameObject[3];

    public GameObject startEffect;
    int m_playingID;

    bool[] m_ShowTutorial = new bool[3];

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < 3; i++)
        {
            m_ShowTutorial[i] = false;
        }

        m_playingID = Random.Range(0, 3);
        
        if(m_ShowTutorial[m_playingID])
        {
            StartTutorial(m_playingID);
        }
        else
        {
            StartGame(m_playingID);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void StartTutorial(int gameID)
    {
        Debug.Log("Start Tutorial");
        StartGame(m_playingID);

        m_playingID = gameID;
    }

    void StartGame(int gameID)
    {
        startEffect.SetActive(true);
        Debug.Log("Game Start");
        miniGameList[gameID].SetActive(true);

        m_playingID = gameID;
    }

    public void GameFinish(GameObject miniGame)
    {
        miniGame.SetActive(false);
        StartCoroutine(GameEndEvent());
    }

    IEnumerator GameEndEvent()
    {
        Debug.Log("Game Clear");
        yield return new WaitForSeconds(3.0f);
        StartNewGame();
    }

    void StartNewGame()
    {
        int gameID = (m_playingID + Random.Range(1, 3)) % 3;
        StartGame(gameID);
    }
}
