﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartEffect : MonoBehaviour
{
    SpriteRenderer m_spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);

        if(m_spriteRenderer == null)
        {
            m_spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        }
        m_spriteRenderer.color = Color.white;

        StartCoroutine(ShowAnimation());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator ShowAnimation()
    {
        while(this.gameObject.transform.localScale.x < 1) {
            float newScale = this.gameObject.transform.localScale.x + 0.15f;
            this.gameObject.transform.localScale = new Vector3(newScale, newScale, newScale);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.4f);

        while(m_spriteRenderer.color.a > 0) {
            Color newColor = m_spriteRenderer.color;
            newColor.a -= 5 * Time.deltaTime;
            m_spriteRenderer.color = newColor;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(3);
        this.gameObject.SetActive(false);
    }

    
}
