using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class ReplyPanel : MonoBehaviour {
    public MsgSystem msgSystem;
    public Text msgContent;
    public TextMeshProUGUI sender;
    public List<GameObject> buttonList;
    MsgSystem.Msg myMsg;

    public void Show(MsgSystem.Msg msg) {
        this.myMsg = msg;
        sender.text = msg.sender;
        msgContent.text = msg.content;
        this.gameObject.SetActive(true);
        for(int i = 0 ; i < 3 ; i++) {
            if(i < msg.answers.Count) {
                buttonList[i].SetActive(true);
                buttonList[i].transform.Find("Text").GetComponent<Text>().text = msg.answers[i];
            }
            else {
                buttonList[i].SetActive(false);
            }
        }
    }

    public void ShowSpam(MsgSystem.Msg msg) {
        this.myMsg = msg;
        sender.text = msg.sender;
        msgContent.text = msg.content;
        this.gameObject.SetActive(true);
        for(int i = 0 ; i < 3 ; i++) {
            if(i == 2) {
                buttonList[i].SetActive(true);
                buttonList[i].transform.Find("Text").GetComponent<Text>().text = "Ok. I understand.";
            }
            else {
                buttonList[i].SetActive(false);
            }
        }
    }

    public void AnswerOnClick(int siblingIndex) {
        if(myMsg.type == MsgSystem.MsgType.Question) {
            if(siblingIndex == myMsg.correctAnswerIndex) {
                
            }
            else {

            }
        }
        Close();
    }

    public void Close() {
        msgSystem.ContinueAllCorroutine();
        this.gameObject.SetActive(false);
    }
}