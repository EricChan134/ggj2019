﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MsgSystem : MonoBehaviour
{
    public enum MsgType {
        Question = 0,
        ChitChat,
        NotImportant,
        Count
    }

    public struct Msg {
        public string sender;
        public string content;
        public MsgType type;
        public List<string> answers;
        public int correctAnswerIndex;
    }

    public MsgData msgData;
    public GameObject notificationPrefab;
    public GameObject replyPanelObj;
    public Transform canvasTrasnform;

    public bool shouldGenerateMsg = true;
    public bool pauseCoroutine = false;

    private void Start() {
        StartCoroutine(GenerateQuestion());
        StartCoroutine(GenerateChitChat());
        StartCoroutine(GenerateNotImportantMsg());
    }

    public void PauseAllCorroutine() {
        pauseCoroutine = true;
    }

    public void ContinueAllCorroutine() {
        pauseCoroutine = false;
    }

    public void NotificationOnClick(Msg msg) {
        if(msg.type != MsgType.NotImportant) {
            PauseAllCorroutine();
            replyPanelObj.GetComponent<ReplyPanel>().Show(msg);
        }
        else {
            replyPanelObj.GetComponent<ReplyPanel>().ShowSpam(msg);
        }
    }

    IEnumerator GenerateQuestion() {
        yield return new WaitForSeconds(5);
        while(shouldGenerateMsg) {
            while(pauseCoroutine) {
                yield return new WaitForEndOfFrame();
            }
            GameObject msgObj = Instantiate(notificationPrefab, canvasTrasnform);
            msgObj.transform.SetSiblingIndex(0);
            Notification noti = msgObj.GetComponent<Notification>();
            noti.msgSystem = this;
            Msg msg = new Msg();
            int randomIndex = Random.Range(0,msgData.questionList.Count);
            msg.content = msgData.questionList[randomIndex];
            msg.sender = "Fiancé";
            msg.type = MsgType.Question;
            string separator = ";";
            List<string> answers = new List<string>(msgData.answersList[randomIndex].Split(separator.ToCharArray()));
            msg.answers = answers;
            msg.correctAnswerIndex = msgData.correctAnswerIndexList[randomIndex];
            noti.UpdateMsgContent(msg);
            noti.Show();
            int time = Random.Range(22,32);
            yield return new WaitForSeconds(time);
        }
    }

    IEnumerator GenerateChitChat() {
        while(shouldGenerateMsg) {
            while(pauseCoroutine) {
                yield return new WaitForEndOfFrame();
            }
            int time = Random.Range(15,20);
            yield return new WaitForSeconds(time);
            GameObject msgObj = Instantiate(notificationPrefab, canvasTrasnform);
            msgObj.transform.SetSiblingIndex(0);
            Notification noti = msgObj.GetComponent<Notification>();
            noti.msgSystem = this;
            Msg msg = new Msg();
            int randomIndex = Random.Range(0,msgData.chitChatList.Count);
            msg.content = msgData.chitChatList[randomIndex];
            msg.sender = "Fiancé";
            msg.type = MsgType.ChitChat;
            string separator = ";";
            List<string> answers = new List<string>(msgData.chitChatResponseList[randomIndex].Split(separator.ToCharArray()));
            msg.answers = answers;
            noti.UpdateMsgContent(msg);
            noti.Show();
        }
    }

    IEnumerator GenerateNotImportantMsg() {
        while(shouldGenerateMsg) {
            while(pauseCoroutine) {
                yield return new WaitForEndOfFrame();
            }
            int time = Random.Range(10,15);
            yield return new WaitForSeconds(time);
            GameObject msgObj = Instantiate(notificationPrefab, canvasTrasnform);
            msgObj.transform.SetSiblingIndex(0);
            Notification noti = msgObj.GetComponent<Notification>();
            noti.msgSystem = this;
            Msg msg = new Msg();
            int randomIndex = Random.Range(0,msgData.otherTextList.Count);
            msg.content = msgData.otherTextList[randomIndex];
            msg.sender =  msgData.otherSenderList[randomIndex];
            msg.type = MsgType.NotImportant;
            noti.UpdateMsgContent(msg);
            noti.Show();
        }
    }
}
