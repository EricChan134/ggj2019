﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MCAnsButton : MonoBehaviour
{
    public ReplyPanel replyPanel;
    public Text buttonText;
    
    public void OnClick() {
        replyPanel.AnswerOnClick(transform.GetSiblingIndex());
    }
}
