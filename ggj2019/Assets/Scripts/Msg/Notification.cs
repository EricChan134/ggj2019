using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Notification : MonoBehaviour {
    public MsgSystem msgSystem;
    public TextMeshProUGUI senderText;
    public Text msgContentText;
    public float selfDestructTime;
    public CanvasGroup cg;
    
    MsgSystem.Msg msg;

    public void Show() {
        StartCoroutine(ShowAnimation());
        StartCoroutine(SelfDestroy());
    }

    public void UpdateSender(string name) {
        senderText.text = name;
    }

    public void UpdateMsgContent(MsgSystem.Msg msg) {
        this.msg = msg;
        if(msg.sender != "") {
            senderText.text = msg.sender;
        }
        msgContentText.text = msg.content;
    }

    public void NotificationOnClick() {
        msgSystem.NotificationOnClick(msg);
        StartCoroutine(ImmediateSelfDestroy());
    }

    IEnumerator ShowAnimation() {
        this.gameObject.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
        while(this.gameObject.transform.localScale.x < 1) {
            float newScale = this.gameObject.transform.localScale.x + 0.02f;
            this.gameObject.transform.localScale = new Vector3(newScale, newScale, newScale);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator SelfDestroy() {
        yield return new WaitForSeconds(selfDestructTime);
        cg.interactable = false;
        while(cg.alpha > 0) {
            cg.alpha -= 0.1f;
            yield return new WaitForEndOfFrame();
        }
        Destroy(this.gameObject);
    }

     IEnumerator ImmediateSelfDestroy() {
        cg.interactable = false;
        while(cg.alpha > 0) {
            cg.alpha -= 0.1f;
            yield return new WaitForEndOfFrame();
        }
        Destroy(this.gameObject);
    }
}